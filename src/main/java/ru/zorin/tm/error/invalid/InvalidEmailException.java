package ru.zorin.tm.error.invalid;

import ru.zorin.tm.error.AbstractException;

public class InvalidEmailException extends AbstractException {

    public InvalidEmailException() {
        super("Email is invalid");
    }
}