package ru.zorin.tm.error.project;

public class ProjectUpdateException extends RuntimeException{

    public ProjectUpdateException() {
        super("Fail to update project");
    }
}