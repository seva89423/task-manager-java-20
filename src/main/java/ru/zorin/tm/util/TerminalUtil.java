package ru.zorin.tm.util;

import ru.zorin.tm.error.invalid.InvalidIndexException;
import ru.zorin.tm.error.invalid.InvalidNameException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine(){
        try {
            return SCANNER.nextLine();
        } catch (Exception e){
            throw new InvalidNameException();
        }
    }

    static Integer nextNumber() {
        final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (Exception e){
            throw new InvalidIndexException();
        }
    }
}