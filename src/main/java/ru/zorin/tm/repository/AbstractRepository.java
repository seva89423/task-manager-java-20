package ru.zorin.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.zorin.tm.api.repository.IRepository;
import ru.zorin.tm.entity.AbstractEntity;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    private List<E> records = new ArrayList<>();

    public List<E> findAll(){
        return records;
    }

    public void clear() {
        records.removeAll(records);
    }

    public void add(@NotNull final List<E> list) {
        for (final E e : list) {
            if (e == null) return;
            list.add(e);
        }
    }

    public void load(final List<E> list) {
        add(list);
    }
}