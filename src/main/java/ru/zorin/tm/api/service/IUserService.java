package ru.zorin.tm.api.service;

import ru.zorin.tm.entity.Project;
import ru.zorin.tm.entity.User;
import ru.zorin.tm.role.Role;

import java.util.List;

public interface IUserService extends IService<User>{

    List<User> findAll();

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findById(String userId);

    User findByLogin(String login);

    User updateById(String userId, String id, String login, String password);

    User updateByLogin(String userId, String login, String password);

    User removeUser(String userId, User user);

    User removeById(String userId, String id);

    User removeByLogin(String userId, String login);

    User lockServiceByLogin(String login);

    User unlockServiceByLogin(String login);
}