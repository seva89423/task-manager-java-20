package ru.zorin.tm.api.service;

import ru.zorin.tm.entity.Task;

import java.util.List;

public interface ITaskService extends IService<Task>{

    void create(String userId, String name);

    void create(String userId, String name, String description);

    void add(String userId, Task task);

    void remove(String userId, Task task);

    List<Task> findAll(String userId);

    void clear(String userId);

    Task findOneById(String userId, String id);

    Task findOneByIndex(String userId, Integer index);

    Task findOneByName(String userId, String name);

    Task removeOneByName(String userId, String name);

    Task removeOneByIndex(String userId, Integer index);

    Task removeOneById(String userId, String id);

    Task updateTaskById(String userId, String id, String name, String description);

    Task updateTaskByIndex(String userId, Integer index, String name, String description);
}
