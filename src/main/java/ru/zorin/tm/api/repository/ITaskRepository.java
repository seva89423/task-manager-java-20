package ru.zorin.tm.api.repository;

import ru.zorin.tm.entity.Project;
import ru.zorin.tm.entity.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task>{

    void add (String userId, Task task);

    void remove(String userId, Task task);

    void load(final List<Task> tasks);

    void add(final List<Task> task);

    List<Task> findAll();

    List<Task> findAllTask(String userId);

    void clear();

    Task findOneById(String userId, String id);

    Task findOneByIndex(String userId, Integer index);

    Task getTaskByName(String userId, String name);

    Task removeOneByName(String userId, String name);

    Task removeOneByIndex(String userId, Integer index);

    Task removeOneById(String userId, String id);
}
