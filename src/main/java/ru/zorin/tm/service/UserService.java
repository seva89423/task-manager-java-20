package ru.zorin.tm.service;

import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.api.repository.IUserRepository;
import ru.zorin.tm.api.service.IUserService;
import ru.zorin.tm.entity.User;
import ru.zorin.tm.error.invalid.*;
import ru.zorin.tm.error.task.TaskEmptyException;
import ru.zorin.tm.role.Role;
import ru.zorin.tm.util.HashUtil;

import java.util.List;

public class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(final String userId) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        return userRepository.findById(userId);
    }


    @Override
    public void load(final List<User> usersList) {
        if (usersList == null) return;
        userRepository.load(usersList);
    }

    @Override
    public List<User> getList() {
        return userRepository.getUsersList();
    }


    @Override
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new InvalidLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public User removeUser(String userId, final User user) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        if (user == null) return null;
        return userRepository.removeUser(user);
    }

    @Override
    public User removeById(String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        if (id == null || id.isEmpty()) throw new InvalidIdException();
        return userRepository.removeById(id);
    }

    @Override
    public User removeByLogin(String userId, final String login) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        if (login == null || login.isEmpty()) throw new InvalidLoginException();
        return userRepository.removeByLogin(login);
    }

    @Nullable
    @Override
    public User create(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new InvalidLoginException();
        if (password == null || password.isEmpty()) throw new InvalidPasswordException();
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPassword(HashUtil.salt(password));
        return userRepository.add(user);
    }

    @Nullable
    @Override
    public User create(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) throw new InvalidLoginException();
        if (password == null || password.isEmpty()) throw new InvalidPasswordException();
        if (email == null || email.isEmpty()) throw new InvalidEmailException();
        final User user = create(login, password);
        if (user == null) return null;
        user.setEmail(email);
        return user;
    }

    @Nullable
    @Override
    public User create(final String login, final String password, final Role role) {
        if (login == null || login.isEmpty()) throw new InvalidLoginException();
        if (password == null || password.isEmpty()) throw new InvalidPasswordException();
        if (role == null) throw new InvalidRoleException();
        final User user = create(login, password);
        if (user == null) return null;
        user.setRole(role);
        return user;
    }

    @Nullable
    @Override
    public User updateById(final String userId, final String id, final String login, final String password) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        if (id == null || id.isEmpty()) throw new InvalidIdException();
        if (login == null || login.isEmpty()) throw new InvalidNameException();
        final User user = findById(userId);
        if (user == null) throw new TaskEmptyException();
        user.setId(id);
        user.setLogin(login);
        user.setPassword(password);
        return user;
    }
    @Nullable
    @Override
    public User updateByLogin(final String userId, final String login, final String password) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        if (login == null || login.isEmpty()) throw new InvalidNameException();
        final User user = findById(userId);
        if (user == null) throw new TaskEmptyException();
        user.setLogin(login);
        user.setPassword(password);
        return user;
    }

    @Nullable
    @Override
    public User lockServiceByLogin(String login) {
        if (login == null || login.isEmpty()) throw new InvalidNameException();
        final User user = findByLogin(login);
        if (user == null) throw new InvalidNameException();
        user.setLocked(true);
        return user;
    }

    @Nullable
    @Override
    public User unlockServiceByLogin(String login) {
        if (login == null || login.isEmpty()) throw new InvalidNameException();
        final User user = findByLogin(login);
        if (user == null) throw new InvalidNameException();
        user.setLocked(false);
        return user;
    }
}