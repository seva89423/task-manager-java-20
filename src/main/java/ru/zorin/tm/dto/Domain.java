package ru.zorin.tm.dto;

import lombok.Getter;
import lombok.Setter;
import ru.zorin.tm.entity.Project;
import ru.zorin.tm.entity.Task;
import ru.zorin.tm.entity.User;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@XmlRootElement
public class Domain implements Serializable {

    private List<Task> tasks = new ArrayList<>();

   /* public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }*/

    private List<Project> projects = new ArrayList<>();

    private List<User> users = new ArrayList<>();
}