package ru.zorin.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.dto.Domain;

import java.io.Serializable;

public abstract class AbstractDataCommand extends AbstractCommand implements Serializable {

    public AbstractDataCommand() {

    }

    @Nullable
    public Domain getDomain() {
        final Domain domain = new Domain();
        domain.setProjects(serviceLocator.getProjectService().getList());
        domain.setTasks(serviceLocator.getTaskService().getList());
        domain.setUsers(serviceLocator.getUserService().getList());
        return domain;
    }

    @Nullable
    public void setDomain(final Domain domain) {
        if (domain == null) return;
        serviceLocator.getProjectService().getList().clear();
        serviceLocator.getProjectService().load(domain.getProjects());
        serviceLocator.getTaskService().getList().clear();
        serviceLocator.getTaskService().load(domain.getTasks());
        serviceLocator.getUserService().getList().clear();
        serviceLocator.getUserService().load(domain.getUsers());
    }

    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return null;
    }

    @Nullable
    @Override
    public void execute() throws Exception {

    }
}
