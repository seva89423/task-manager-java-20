package ru.zorin.tm.command.data.bin;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.command.AbstractDataCommand;
import ru.zorin.tm.constant.DataConst;
import ru.zorin.tm.dto.Domain;
import ru.zorin.tm.role.Role;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;

public class DataBinSaveCommand extends AbstractDataCommand {
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-bin-save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to bin file";
    }

    @Nullable
    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BIN SAVE]");
        final Domain domain = getDomain();
        final File file = new File(DataConst.FILE_BIN);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        final FileOutputStream fileOutputStream = new FileOutputStream(file);
        final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.flush();
        fileOutputStream.close();
        System.out.println("[COMPLETE]");
    }

    public Role[] roles(){
        return new Role[] { Role.ADMIN };
    }
}